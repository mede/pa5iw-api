const Stripe = require('stripe');
const stripe = Stripe('sk_test_51IKRbWDvWvbWxWR7ojrpmcYQprklo9AFc7Bqw0cv95rr5u9MJydFRDvVkMS50sVrCm9Xyjf1Iwx7Dx606VzJeCDm00XVcSPFJb');
const express = require("express");
const models = require('../models/index');
const User = models.User

const router = express.Router();

router.post("/checkout_succeeded", (req, res) => {
    customerStripeId = req.body.data.object.customer
    subId = req.body.data.object.id
    stripe.customers.retrieve(customerStripeId)
    .then(customer => {
        console.log(customer)
        const email = customer.email
        User.findOne({
            where: {
                email: email
            }
        }).then((user) => {
            User.update({account_type: "Premium", sub_id: subId}, {where: {id: user.id}})
                .then(() => res.sendStatus(200))
                .catch(() => res.sendStatus(404))
        }).catch(err => {
            console.log(`No user found for email : ${email}`)
            return res.sendStatus(404)
        })
    })
});

router.post("/subcription_cancel", (req, res) => {
    const email = req.body.email
    User.findOne({
        where: {
            email: email
        }
    }).then(async (user) => {
        stripe.subscriptions.del(user.sub_id).then(subscription => {
            console.log(subscription)
            User.update({account_type: "free", sub_id: ''}, {where: {id: user.id}})
                .then(() => res.sendStatus(200))
                .catch(() => res.sendStatus(404))
        })
    }).catch(err => {
        console.log(`No user found for email : ${email}`)
        return res.sendStatus(404)
    })
});

module.exports = router;