const express = require("express");
const models = require('../models/index');
const User = models.User
const Project = models.Project
const Role = models.Role

const router = express.Router();

// ajouter role user à un projet
router.post('/create/', (req, res) => {
    Project.findByPk(req.body.project_id)
        .then((project) => {
            if (!project) {
                console.log("project not found!");
                return null;
            } else {
                return User.findByPk(req.body.user_id).then((user) => {
                    if (!user) {
                        console.log("User not found!");
                        return null;
                    } else {
                        Role.findAll({
                            where: {user_id: req.body.user_id, project_id: req.body.project_id, role: req.body.role}
                        }).then((result) => {
                            if (result && result.length) {
                                console.log('error ! same user role & project')
                                return null
                            } else {
                                return Role.create(req.body)
                                    .then((data) => {
                                        res.status(201).json(data)
                                    })
                                    .catch((error) => {
                                            error.error
                                        }
                                    );
                            }
                        }).catch(error => {
                            error.error
                        })
                    }
                });
            }
        }).catch(error => {
        error.error
    })
});


router.put("/:id", (req, res) => {
    Role.update(req.body, {where: {id: req.params.id}, returning: true})
        .then(() => res.sendStatus(200))
        .catch((err) => res.sendStatus(500));
});

router.delete("/:id", (req, res) => {
    Role.destroy({
        where: {
            id: req.params.id,
        },
    })
        .then((nbDelete) => (nbDelete ? res.sendStatus(204) : res.sendStatus(404)))
        .catch((res) => res.sendStatus(500));
});
module.exports = router;
