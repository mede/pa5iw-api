const models = require('../models/index');
const Message = models.Message;

module.exports = function(client, io) {
    client.on('message-send', (data) => {
        console.log(data)
        Message.create({ProjectId: data.message.idProject, from: data.message.value.from, content: data.message.value.content})
        io.to(data.slug).emit('message-send-back', data.message)
    });
};
