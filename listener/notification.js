const models = require('../models/index');
const User = models.User
const Notification = models.Notification

module.exports = function(socket, io) {
    socket.on('notification-add-user', (data) => {
        User.findOne({ where: {email: data.user_to_notify}})
        .then((user) => {
            if (!user) {
                console.log("User not found!");
                return null;
            } else {
                Notification.create({
                    user_to_notify : user.id,
                    content : data.content,
                    fromUsername: data.fromUsername
                })
                .then((notification) => {
                    io.to(data.user_to_notify).emit('notification-emit-add-user', notification)
                })
            }
        })
    });

    socket.on('notification-remove-user', (data) => {
        User.findOne({ where: {email: data.user_to_notify}})
        .then((user) => {
            if (!user) {
                console.log("User not found!");
                return null;
            } else {
                Notification.create({
                    user_to_notify : user.id,
                    content : data.content,
                    fromUsername: data.fromUsername
                })
                .then((notification) => {
                    io.to(data.user_to_notify).emit('notification-emit-add-user', notification)
                })
            }
        })
    });
  
  };