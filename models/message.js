'use strict';
const {
    Model
} = require('sequelize');

module.exports = (sequelize, DataTypes) => {
    class Message extends Model {

        static associate(models) {
            Message.belongsTo(models.Project)
        }
    };
    Message.init({
        id: {
            type: DataTypes.INTEGER,
            autoIncrement: true,
            primaryKey: true
        },
        content: {
            type: DataTypes.TEXT
        },
        from: {
            type: DataTypes.STRING
        }
    }, {
        sequelize,
        modelName: 'Message',
    });
    return Message;
};