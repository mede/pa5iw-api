'use strict';

const {
    Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    class Project extends Model {

        static associate(models) {
            Project.belongsToMany(models.User, {
                through: "user_project",
                as: "users",
                foreignKey: "project_id",
                onDelete: 'CASCADE',
            });
            Project.belongsTo(models.User, {
                foreignKey: 'createdBy',
                onDelete: 'CASCADE',
            });
            Project.hasOne(models.Code, {
                onDelete: 'CASCADE'
            });
            Project.hasMany(models.Message)
        }
    };
    Project.init({
        id: {
            type: DataTypes.INTEGER,
            autoIncrement: true,
            primaryKey: true
        },
        name: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        capacity: {
            type: DataTypes.STRING,
            allowNull: false,
            defaultValue: 5,
        },
        lang: {
            allowNull: false,
            type: DataTypes.ENUM({
                values: ['js']
            }),
            defaultValue: 'js'
        },
        slug: {
            allowNull: false,
            type: DataTypes.STRING
        }
    }, {
        sequelize,
        modelName: 'Project',
    });
    return Project;
};