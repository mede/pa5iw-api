'use strict';
const {
    Model
} = require('sequelize');
const bcrypt = require("bcryptjs");

module.exports = (sequelize, DataTypes) => {
    class User extends Model {

        static associate(models) {
            User.belongsToMany(models.Project, {
                through: "user_project",
                as: "projects",
                foreignKey: "user_id",
                onDelete: 'CASCADE',
            })
            User.hasMany(models.Role, {
                as: "role",
                foreignKey: "user_id",
                onDelete: 'CASCADE',
            })
        }
    };
    User.init({
        id: {
            type: DataTypes.INTEGER,
            autoIncrement: true,
            primaryKey: true
        },
        username: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        email: {
            type: DataTypes.STRING,
            allowNull: false,
            unique: true,
            validate: {
                isEmail: {msg: "Email invalid"},
            },
        },
        password: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        role_app: {
            allowNull: false,
            type: DataTypes.ENUM({
                values: ['admin', 'user']
            }),
            defaultValue: 'user'
        },
        account_type: {
            type: DataTypes.STRING,
            allowNull: false,
            defaultValue: 'free' 
        },
        sub_id: {
            type: DataTypes.STRING,
            allowNull: true,
        }
    }, {
        sequelize,
        modelName: 'User',
    });

    User.addHook("beforeCreate", async (user, options) => {
        const salt = await bcrypt.genSalt(10);
        user.password = await bcrypt.hash(user.password, salt);
    });

    return User;
};